package org.handsomeking.mincroservice.openfein8031.controller;

import org.handsomeking.mincroservice.openfein8031.service.Service1Client;
import org.handsomeking.mincroservice.openfein8031.service.Service2Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/***
 * @author gaoqijin
 * @date 2021-2-22
 * @Description
 */
@RestController
@RequestMapping("/openFein")
public class IndexController {

    @Autowired
    private Service2Client service2Client;
    @Autowired
    private Service1Client service1Client;
    @GetMapping("/getPort")
    public String getPort(HttpServletRequest request){
        System.out.println(">>>>>>>>>>>>"+request.getHeader("token"));
        return service2Client.info();
    }

    @GetMapping("/getPort2")
    public String getPort2(){
        return service1Client.info();
    }


}
