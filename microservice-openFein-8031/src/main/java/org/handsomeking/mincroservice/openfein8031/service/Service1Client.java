package org.handsomeking.mincroservice.openfein8031.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/***
 * @author gaoqijin
 * @date 2021-3-5
 * @Description
 */
@FeignClient("server1")
@Component
public interface Service1Client {


    @GetMapping("/config/getPort")
    String info();

}
