package org.handsomeking.mincroservice.openfein8031.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/***
 * @author gaoqijin
 * @date 2021-2-22
 * @Description
 */
@FeignClient("server2")
@Component
public interface Service2Client {

    @GetMapping("/getPort")
    String info();
}
