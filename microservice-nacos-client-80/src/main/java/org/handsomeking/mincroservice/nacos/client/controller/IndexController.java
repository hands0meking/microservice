package org.handsomeking.mincroservice.nacos.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/***
 * @author gaoqijin
 * @date 2021-6-3
 * @Description
 */
@RestController("/client")
public class IndexController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public String index(){
        return "hello";
    }

    @GetMapping("/port")
    public String getPort(){
        System.out.println("client ... 请求");
        return restTemplate.getForEntity("http://nacos-service1/server1/port",String.class).getBody();
    }
}
