package org.handsomeking.mincroservice.nacos.client.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/***
 * @author gaoqijin
 * @date 2021-6-9
 * @Description
 */
@RestController
@RequestMapping("/sentinel")
public class SentinelController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/port")
    public String getPort(){
        return restTemplate.getForEntity("http://nacos-sentinel/sentinel/port",String.class).getBody();
    }

    @GetMapping("/sayHi")
    public String sayHi( ) {
        return restTemplate.getForEntity("http://nacos-sentinel/sentinel/sayHi",String.class).getBody();
    }

    @GetMapping("/hotKeyTest")
    @SentinelResource(value = "hotKeyTest",blockHandler = "blockHandler",fallback = "fallback")
    public String hotKeyTest(){
        return "hotKeyTest OK ";
    }
    private String blockHandler(){
        return "blockHandler   .... ";
    }

    private String fallback(){
        return "fallback   .... ";
    }
}
