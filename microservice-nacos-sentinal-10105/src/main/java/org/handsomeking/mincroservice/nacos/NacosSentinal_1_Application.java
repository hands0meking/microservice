package org.handsomeking.mincroservice.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/***
 * @author gaoqijin
 * @date 2021-2-23
 * @Description
 */
@SpringBootApplication
public class NacosSentinal_1_Application {

    public static void main(String[] args) {
        SpringApplication.run(NacosSentinal_1_Application.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
