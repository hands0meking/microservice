package org.handsomeking.mincroservice.nacos.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/***
 * @author gaoqijin
 * @date 2021-6-15
 * @Description
 */
@FeignClient(name = "nacos-service1")
@Component
public interface Server1Client {

    @GetMapping("/server1/sayHi")
    String sayHi();

}
