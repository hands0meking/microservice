package org.handsomeking.mincroservice.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/***
 * @author gaoqijin
 * @date 2021-6-11
 * @Description
 */
@SpringBootApplication
@EnableFeignClients
public class SentinelFeinApplication {
    public static void main(String[] args) {
        SpringApplication.run(SentinelFeinApplication.class, args);
    }
}
