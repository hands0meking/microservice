package org.handsomeking.mincroservice.nacos.controller;

import org.handsomeking.mincroservice.nacos.client.Server1Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * @author gaoqijin
 * @date 2021-6-15
 * @Description
 */
@RestController
@RequestMapping("/feign")
public class FeignController {

    @Autowired
    private Server1Client server1Client;

    @Value("${server.port:8888}")
    private int port;

    @GetMapping(value = "/port")
    public String get( ) {
        return "port : "+ port;
    }

    @GetMapping("/sayHi")
    public String testA(){
        return server1Client.sayHi();
    }

}
