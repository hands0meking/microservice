package org.handsomeking.mincroservice.service8011;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/***
 * @author gaoqijin
 * @date 2021-2-22
 * @Description
 */
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class Server18022Application {

    public static void main(String[] args) {
        SpringApplication.run(Server18022Application.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  Server18022Application 启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " .-------.       ____     __        \n" +
                " |  _ _   \\      \\   \\   /  /    \n" +
                " | ( ' )  |       \\  _. /  '       \n" +
                " |(_ o _) /        _( )_ .'         \n" +
                " | (_,_).' __  ___(_ o _)'          \n" +
                " |  |\\ \\  |  ||   |(_,_)'         \n" +
                " |  | \\ `'   /|   `-'  /           \n" +
                " |  |  \\    /  \\      /           \n" +
                " ''-'   `'-'    `-..-'              ");
    }

}
