package org.handsomeking.mincroservice.service8011.controller;

import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/***
 * @author gaoqijin
 * @date 2021-2-22
 * @Description
 */
@RestController
@RequestMapping("/server2")
public class IndexController {

    @Value("${server.port}")
    private int port;

    @GetMapping("getPort")
    public String info(HttpServletRequest request){
        System.out.println(">>>>>>>>>>>>"+request.getHeader("token"));
        return LocalDateTime.now() + " >> "+port;
    }
    /**
     * 测试ribbon的超时配置
     * @return
     */
    @GetMapping("/timeout")
    public String timeout(){
        System.out.println(LocalDateTime.now()+"IndexController.info timeout");
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return LocalDateTime.now() + " >> "+port;
    }
    @Autowired
    private DiscoveryClient discoveryClient;



    @GetMapping("/discovery")
    public Object index(){
        return discoveryClient;
    }


}
