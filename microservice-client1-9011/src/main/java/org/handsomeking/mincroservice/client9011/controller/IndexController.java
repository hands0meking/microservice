package org.handsomeking.mincroservice.client9011.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/***
 * @author gaoqijin
 * @date 2021-2-22
 * @Description
 */
@RestController
@RequestMapping("/client")
public class IndexController {


    @Autowired
    private RestTemplate restTemplate;

    private String url = "http://SERVER2/getPort";
    private String url2 = "http://SERVER1/config/getPort";

    @GetMapping("getPort")
    public String getServerPort(){
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        return response.getBody();
    }

    @GetMapping("getPort2")
    public String getServerPort2(){
        ResponseEntity<String> response = restTemplate.getForEntity(url2, String.class);
        return response.getBody();
    }


}
