package org.handsomeking.mincroservice.irule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/***
 * @author gaoqijin
 * @date 2021-3-5
 * @Description
 */
@Configuration
public class MyIRuleConfig {


    @Bean
    public IRule myIRule(){
        return new RandomRule();
    }

}
