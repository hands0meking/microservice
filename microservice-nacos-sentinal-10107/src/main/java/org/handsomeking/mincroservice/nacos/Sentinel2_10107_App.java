package org.handsomeking.mincroservice.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/***
 * @author gaoqijin
 * @date 2021-6-15
 * @Description
 */
@SpringBootApplication
public class Sentinel2_10107_App {
    public static void main(String[] args) {
        SpringApplication.run(Sentinel2_10107_App.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
