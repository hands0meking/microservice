package org.handsomeking.mincroservice.nacos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author gaoqijin
 */
@RestController
@RequestMapping("/sentinel")
public class IndexController {

    @Value("${server.port:8888}")
    private int port;

    @Value("${spring:application:name}")
    private String serverName;
    @Autowired
    private RestTemplate restTemplate;


    @GetMapping(value = "/port")
    public String get( ) {
        return "port : "+ port;
    }

    @GetMapping("/sayHi")
    public String sayHi( ) {
        String body = restTemplate.getForEntity("http://nacos-service1/server1/sayHi", String.class).getBody();
        return  body +" >> " + serverName + "sayHi (#^.^#) ";
    }


    @GetMapping("/testA")
    public String testA(){
        return "testA!";
    }

    @GetMapping("/testB")
    public String testB(){
        return "testB!";
    }
}
