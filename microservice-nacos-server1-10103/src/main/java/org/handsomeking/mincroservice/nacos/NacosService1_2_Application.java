package org.handsomeking.mincroservice.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/***
 * @author gaoqijin
 * @date 2021-2-23
 * @Description
 */
@SpringBootApplication
public class NacosService1_2_Application {

    public static void main(String[] args) {
        SpringApplication.run(NacosService1_2_Application.class, args);
    }
}
