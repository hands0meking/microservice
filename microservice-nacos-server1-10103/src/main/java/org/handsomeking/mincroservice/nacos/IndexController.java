package org.handsomeking.mincroservice.nacos;

import com.alibaba.nacos.api.exception.NacosException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/server1")
public class IndexController {

    @Value("${server.port:8888}")
    private int port;

    @Value("${spring:application:name}")
    private String serverName;

    @GetMapping(value = "/port")
    public String get( ) {
        return "port : "+ port;
    }

    @GetMapping("/sayHi")
    public String sayHi( ) {
        return  serverName + "【"+ port+"】" + "sayHi (#^.^#) ";
    }
}
