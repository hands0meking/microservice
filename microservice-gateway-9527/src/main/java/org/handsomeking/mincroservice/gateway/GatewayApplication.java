package org.handsomeking.mincroservice.gateway;

import org.handsomeking.mincroservice.gateway.filter.MyFilter;
import org.handsomeking.mincroservice.gateway.filter.MyFilter2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

/***
 * @author gaoqijin
 * @date 2021-4-21
 * @Description
 */
@EnableEurekaClient
@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }


    @Bean
    public MyFilter myFilter(){
        return new MyFilter();
    }

    @Bean
    public MyFilter2 myFilter2(){
        return new MyFilter2();
    }

}
