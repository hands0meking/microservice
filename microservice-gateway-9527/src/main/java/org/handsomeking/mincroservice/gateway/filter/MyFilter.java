package org.handsomeking.mincroservice.gateway.filter;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.annotation.Order;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/***
 * @author gaoqijin
 * @date 2021-4-21
 * @Description
 * 全局过滤器
 */
@Order(2)
public class MyFilter  implements GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println("MyFilter.filter ... ");
        return chain.filter(exchange);
    }
}
