package org.handsomeking.mincroservice.service8011;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {

    @Value("${useLocalCache:false}")
    private boolean useLocalCache;
    @Value("${server.port}")
    private int port;
    
    @Value("${server.tomcat.maxThreads:0}")
    private int maxThread;
    @Value("${server.tomcat.maxConnections:0}")
    private int maxConnections;
    /**
     * http://localhost:8080/config/get
     */
    @RequestMapping("/get")
    public boolean get() {
        return useLocalCache;
    }

    @GetMapping("getPort")
    public String info(){
        return LocalDateTime.now() + " >> "+port +" >> "+maxThread +" >> "+maxConnections;
    }

    /**
     * 测试ribbon的超时配置
     * @return
     */
    @GetMapping("/timeout")
    public String timeout(){
        System.out.println(LocalDateTime.now()+"IndexController.info timeout");
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return LocalDateTime.now() + " >> "+port;
    }
}
