package org.handsomeking.mincroservice.hystrixDashboard.client;

import org.handsomeking.mincroservice.hystrixDashboard.client.fallback.Service2ClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/***
 * @author gaoqijin
 * @date 2021-3-9
 * @Description
 */
@Component
@FeignClient(value = "SERVER2",fallback = Service2ClientFallback.class)
public interface Service2Client {
    @GetMapping("/timeout")
    String timeout();

    @GetMapping("/discovery")
    Object index();
}
