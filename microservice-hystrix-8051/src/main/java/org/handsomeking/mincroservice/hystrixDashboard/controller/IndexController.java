package org.handsomeking.mincroservice.hystrixDashboard.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/***
 * @author gaoqijin
 * @date 2021-3-5
 * @Description
 */
@RestController
@RequestMapping("/hystrix")
public class IndexController {


    @GetMapping
    public String normal(){
        return "normal O(∩_∩)O哈哈~";
    }

    @GetMapping("/timeout")
    @HystrixCommand(fallbackMethod = "timeoutFallback",commandProperties = {
            @HystrixProperty(name= HystrixPropertiesManager.EXECUTION_ISOLATION_THREAD_TIMEOUT_IN_MILLISECONDS,value = "3000")
    })
    public String timeout(){
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "normal";
    }
    @GetMapping("/error")
    @HystrixCommand(fallbackMethod = "errorFallback")
    public String error(){
        System.out.println(10/0);
        return "normal";
    }
    public String timeoutFallback(){
        return "IndexController .... timeoutFallback o(╥﹏╥)o ";
    }
    public String errorFallback(){
        return "IndexController .... errorFallback o(╥﹏╥)o";
    }
}
