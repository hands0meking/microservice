package org.handsomeking.mincroservice.hystrixDashboard.controller;

import org.handsomeking.mincroservice.hystrixDashboard.client.Service2Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * @author gaoqijin
 * @date 2021-3-11
 * @Description
 */
@RestController
@RequestMapping("/hystix/feign")
public class OpenFeignClientController {

    @Autowired
    private Service2Client service2Client;
    /**
     * 正常访问
     * @return
     */
    @GetMapping
    public Object index(){
        return service2Client.index();
    }


    @GetMapping("/timeout")
    public String timeout(){
        return service2Client.timeout();
    }

}
