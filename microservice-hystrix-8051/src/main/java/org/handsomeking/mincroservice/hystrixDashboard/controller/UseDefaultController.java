package org.handsomeking.mincroservice.hystrixDashboard.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.hystrix.contrib.javanica.conf.HystrixPropertiesManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/***
 * @author gaoqijin
 * @date 2021-3-9
 * @Description
 */
@RestController
@RequestMapping("/hystrix/default")
@DefaultProperties(defaultFallback = "defaultFallBack",
        commandProperties = {
                @HystrixProperty(name= HystrixPropertiesManager.EXECUTION_ISOLATION_THREAD_TIMEOUT_IN_MILLISECONDS,value = "3000")
                }
    )
public class UseDefaultController {

    @GetMapping("/test1")
    public String test1(){
        return "ok";
    }
    @GetMapping("/test2")
    @HystrixCommand
    public String test2(){
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "使用默认的兜底方法";
    }

    @GetMapping("/test3")
    @HystrixCommand(fallbackMethod = "custFallBack",  commandProperties = {
            @HystrixProperty(name= HystrixPropertiesManager.EXECUTION_ISOLATION_THREAD_TIMEOUT_IN_MILLISECONDS,value = "6000")
    })
    public String test3(){
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "自定义的 fallback!";
    }

    @GetMapping("/test4")
    @HystrixCommand
    public String test4(){
        System.out.println(1/0);
        return "使用默认的兜底方法";
    }
    public String defaultFallBack(){
        return "这个是controller层的default fallback！";
    }
    public String custFallBack(){
        return "这个是controller层的自定义的 fallback！";
    }
}
