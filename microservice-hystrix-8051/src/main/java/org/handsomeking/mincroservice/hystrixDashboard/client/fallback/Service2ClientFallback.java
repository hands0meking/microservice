package org.handsomeking.mincroservice.hystrixDashboard.client.fallback;

import org.handsomeking.mincroservice.hystrixDashboard.client.Service2Client;
import org.springframework.stereotype.Component;

/***
 * @author gaoqijin
 * @date 2021-3-10
 * @Description
 */
@Component
public class Service2ClientFallback implements Service2Client {
    @Override
    public String timeout() {
        return "超时啦...... o(╥﹏╥)o";
    }

    @Override
    public Object index() {
        return "程序异常..... o(╥﹏╥)o";
    }
}
